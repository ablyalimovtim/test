<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['as' => 'artist.', 'prefix' => 'artist',], function() {
	include __DIR__ . '/Artist/routes.php';
});

Route::group(['as' => 'place.', 'prefix' => 'place',], function() {
	include __DIR__ . '/Place/routes.php';
});

Route::group(['as' => 'performance.', 'prefix' => 'performance',], function() {
	include __DIR__ . '/Performance/routes.php';
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
