<?php

Route::get('', ['as' => 'index', 'uses' => 'PerformanceController@index']);

Route::get('{id}/show', ['as' => 'show', 'uses' => 'PerformanceController@show']);

Route::get('create', ['as' => 'create', 'uses' => 'PerformanceController@create']);
Route::post('save', ['as' => 'save', 'uses' => 'PerformanceController@save']);

Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'PerformanceController@edit']);
Route::any('{id}/update', ['as' => 'update', 'uses' => 'PerformanceController@update']);

Route::any('{id}/delete', ['as' => 'delete', 'uses' => 'PerformanceController@delete']);