<?php

Route::get('', ['as' => 'index', 'uses' => 'PlaceController@index']);

Route::get('{id}/show', ['as' => 'show', 'uses' => 'PlaceController@show']);

Route::get('create', ['as' => 'create', 'uses' => 'PlaceController@create']);
Route::post('save', ['as' => 'save', 'uses' => 'PlaceController@save']);

Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'PlaceController@edit']);
Route::any('{id}/update', ['as' => 'update', 'uses' => 'PlaceController@update']);

Route::any('{id}/delete', ['as' => 'delete', 'uses' => 'PlaceController@delete']);