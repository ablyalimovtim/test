<?php

Route::get('', ['as' => 'index', 'uses' => 'ArtistController@index']);

Route::get('{id}/show', ['as' => 'show', 'uses' => 'ArtistController@show']);

Route::get('create', ['as' => 'create', 'uses' => 'ArtistController@create']);
Route::post('save', ['as' => 'save', 'uses' => 'ArtistController@save']);

Route::get('{id}/edit', ['as' => 'edit', 'uses' => 'ArtistController@edit']);
Route::any('{id}/update', ['as' => 'update', 'uses' => 'ArtistController@update']);

Route::any('{id}/delete', ['as' => 'delete', 'uses' => 'ArtistController@delete']);