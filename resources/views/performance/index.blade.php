@extends('layouts.app')

@section('content')
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="glyphicon glyphicon-align-justify"></i> Представления
				<a class="pull-right" href="{{ route('performance.create') }}">
				    <i class="glyphicon glyphicon-plus pull-right"></i>
				</a>
			</div>

			<div class="panel-body">
				@include('performance.list')
			</div>
		</div>	
@endsection
