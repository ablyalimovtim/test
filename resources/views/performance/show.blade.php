@extends('layouts.app')

@section('content')
	<table class="table table-condensed table-striped table-bordered">
        <thead>
        	<th><center>ID</center></th>
            <th><center>Имя</center></th> 
            <th><center>Дата</center></th> 
            <th><center>Артист</center></th> 
            <th><center>Сцена</center></th>           
        </thead> 
        <tbody>   
            <tr>
                @foreach ($performance as $item)
                    <td>  
                        <hr><center>{{ $item->id }}</center><hr>                  
                    </td>
                    <td>  
                        <hr><center>{{ $item->name }}</center><hr>                  
                    </td>
                    <td>  
                        <hr><center>{{ $item->date }}</center><hr>                  
                    </td>
                    <td>  
                        <hr><center>{{ $item->a_name }}</center><hr>                  
                    </td>
                    <td>  
                        <hr><center>{{ $item->p_name }}</center><hr>                  
                    </td>
                @endforeach    
            </tr>
        </tbody>
    </table>                
@endsection