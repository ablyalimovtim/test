@extends('layouts.app')

@section('content')
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Добавление представления</h3>
			</div>

			<div class="panel-body">
				<form class="form-horizontal" role="form" action="{{ route('performance.save') }}" method="POST">
			        <input type="hidden" name="_token" value="{{ csrf_token() }}">

			        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
			            <label for="name" class="col-md-4 control-label">Название</label>

			            <div class="col-md-6">
			                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Название">

			                @if ($errors->has('name'))
			                    <span class="help-block">
			                        <strong>{{ $errors->first('name') }}</strong>
			                    </span>
			                @endif
			            </div>
			        </div>  

			        <div class="form-group{{ $errors->has('artist_id') ? ' has-error' : '' }}">
                        <label for="artist_id" class="col-md-4 control-label">Артист</label>
                        <div class="col-md-6">
                            <select class="form-control" name="artist_id" id="artist_id">
                                <option value="" class="form-control">Артист</option>
                                @if ($artists->count())
                                    @foreach($artists as $artist)   
                                        <option value="{{ $artist->id }}" class="form-control">{{ $artist->name }}</option>
                                    @endforeach
                                @endif      
                            </select> 
                        </div> 
                    </div>   

                    <div class="form-group{{ $errors->has('place_id') ? ' has-error' : '' }}">
                        <label for="place_id" class="col-md-4 control-label">Сцена</label>
                        <div class="col-md-6">
                            <select class="form-control" name="place_id" id="place_id">
                                <option value="" class="form-control">Сцена</option>
                                @if ($places->count())
                                    @foreach($places as $place)   
                                        <option value="{{ $place->id }}" class="form-control">{{ $place->name }}</option>
                                    @endforeach
                                @endif      
                            </select> 
                        </div> 
                    </div>  
 
			        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
			            <label for="date" class="col-md-4 control-label">Название</label>

			            <div class="col-md-6">
			                <input id="date" type="text" class="form-control" name="date" value="{{ old('date') }}" required autofocus placeholder="YYY/MM/DD">

			                @if ($errors->has('name'))
			                    <span class="help-block">
			                        <strong>{{ $errors->first('date') }}</strong>
			                    </span>
			                @endif
			            </div>
			        </div>      

			        <div class="well well-sm">
			            <button type="submit" class="btn btn-primary">Cоздать</button>
			            <a class="btn btn-link pull-right" href="{{ route('performance.index') }}">
			                <i class="glyphicon glyphicon-backward"></i> Назад
			            </a>
			        </div>
			    </form>
			</div>    
		</div>    
	</div>    
@endsection