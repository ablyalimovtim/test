@extends('layouts.app')

@section('content')
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"> Редактирование сцены </h3>
			</div>

			<div class="panel-body">
				<form class="form-horizontal" role="form" action="{{ route('place.update', $place->id) }}" method="POST">
			        <input type="hidden" name="_token" value="{{ csrf_token() }}">

			        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
			            <label for="name" class="col-md-4 control-label">Название</label>

			            <div class="col-md-6">
			                <input id="name" type="text" class="form-control" name="name" value="{{ $place->name }}" required autofocus placeholder="Название">

			                @if ($errors->has('name'))
			                    <span class="help-block">
			                        <strong>{{ $errors->first('name') }}</strong>
			                    </span>
			                @endif
			            </div>
			        </div>   

			        <div class="well well-sm">
			            <button type="submit" class="btn btn-primary">Cоздать</button>
			            <a class="btn btn-link pull-right" href="{{ route('place.index') }}">
			                <i class="glyphicon glyphicon-backward"></i> Назад
			            </a>
			        </div>
			    </form>
			</div>    
		</div>    
	</div>    
@endsection