@if ($artist->count())
    <table class="table table-condensed table-striped">
        <tbody>
            @foreach($artist as $item)
                <tr >
                    <td>{{ $item->name }}</td>
                    <td class="text-right">
                        <a class="btn btn-xs btn-info" href="{{ route('artist.show', $item->id) }}"><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a class="btn btn-xs btn-warning" href="{{ route('artist.edit', $item->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                            <form action="{{ route('artist.delete', $item->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                            </form>   
                    </td>
                </tr>         
            @endforeach                
        </tbody>
    </table>
    {!! $artist->render() !!}
@else
    <center>Артистов нет</center>
@endif
