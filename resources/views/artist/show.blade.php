@extends('layouts.app')

@section('content')
	<table class="table table-condensed table-striped table-bordered">
        <thead>
        	<th><center>ID</center></th>
            <th><center>Имя</center></th>           
        </thead> 
        <tbody>   
            <tr>
                <td>  
                    <hr><center>{{ $artist->id }}</center><hr>                  
                </td>
                <td>  
                    <hr><center>{{ $artist->name }}</center><hr>                  
                </td>
            </tr>
        </tbody>
    </table>                
@endsection