@extends('layouts.app')

@section('content')
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="glyphicon glyphicon-align-justify"></i> Артисты
				<a class="pull-right" href="{{ route('artist.create') }}">
				    <i class="glyphicon glyphicon-plus pull-right"></i>
				</a>
			</div>

			<div class="panel-body">
				@include('artist.list')
			</div>
		</div>	
@endsection

