<?php

namespace App\Model\Performance;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
    	'name',
    	'date',
    	'artist_id',
    	'place_id',
    ];

    static $rules = [
        'name' => 'required',
        'date' => 'date',
        'artist_id' => 'required',
        'place_id' => 'required',
    ];
}
