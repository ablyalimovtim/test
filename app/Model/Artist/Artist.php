<?php

namespace App\Model\Artist;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
    	'name',
    ];

    static $rules = [
        'name' => 'required',
    ];

}
