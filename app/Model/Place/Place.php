<?php

namespace App\Model\Place;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
    	'name',
    ];

    static $rules = [
        'name' => 'required',
    ];
}
