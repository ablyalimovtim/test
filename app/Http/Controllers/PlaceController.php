<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Place\Place;

class PlaceController extends Controller
{
    public function index()
    {
        $place = Place::orderBy('id', 'desc')->paginate(10);

        return view('place.index', [
            'place' => $place
        ]);
    }

    public function create()
    {
        return view('place.create');
    }

    public function save(Request $request)
    {
        $this->validate($request, Place::$rules);

        $place = new Place($request->all());

        $place->save();

        return redirect()->route('place.index');
    }

    public function edit($id)
    {
        $place = Place::find($id);

        return view('place.edit', [
            'place' => $place
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, Place::$rules);

        $place = Place::find($id);

        $place->update($request->all());

        return redirect()->route('place.index');
    }

    public function delete($id)
    {
        $place = Place::where('id', $id);
        $place->delete();

        return redirect()->back();
    }

    public function show($id)
    {
    	$place = Place::find($id);

    	return view('place.show', ['place' => $place]);
    }
}
