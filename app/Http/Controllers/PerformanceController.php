<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Performance\Performance;
use App\Model\Artist\Artist;
use App\Model\Place\Place;

use DB;

class PerformanceController extends Controller
{
    public function index()
    {
        $performance = Performance::orderBy('id', 'desc')->paginate(10);

        return view('performance.index', [
            'performance' => $performance
        ]);
    }

    public function create()
    {
        return view('performance.create', [
        	'artists' => Artist::all(),
			'places' => Place::all()
    	]);
    }

    public function save(Request $request)
    { 
        $this->validate($request, Performance::$rules);

        $performance = new Performance($request->all());

        $performance->save();

        return redirect()->route('performance.index');
    }

    public function edit($id)
    {
        $performance = Performance::find($id);
        $artist = Artist::where('id', $performance->artist_id)->first();
        $place = Place::where('id', $performance->place_id)->first();

        return view('performance.edit', [
            'performance' => $performance,
            'artist' => $artist,
            'artists' => Artist::all(),
            'place' => $place,
            'places' => Place::all(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, Performance::$rules);

        $performance = Performance::find($id);

        $performance->update($request->all());

        return redirect()->route('performance.index');
    }

    public function delete($id)
    {
        $performance = Performance::where('id', $id);
        $performance->delete();

        return redirect()->back();
    }

    public function show($id)
    {
    	$performance = DB::table('performances')
    		->leftJoin('artists', 'performances.artist_id', '=', 'artists.id')
    		->leftJoin('places', 'performances.place_id', '=', 'places.id')
    		->select('performances.*', 'artists.name as a_name', 'places.name as p_name')
    		->get(); 

    	return view('performance.show', ['performance' => $performance]);
    }
}
