<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Artist\Artist;

class ArtistController extends Controller
{
    public function index()
    {
        $artist = Artist::orderBy('id', 'desc')->paginate(10);

        return view('artist.index', [
            'artist' => $artist
        ]);
    }

    public function create()
    {
        return view('artist.create');
    }

    public function save(Request $request)
    { 
        $this->validate($request, Artist::$rules);

        $artist = new Artist($request->all());

        $artist->save();

        return redirect()->route('artist.index');
    }

    public function edit($id)
    {
        $artist = Artist::find($id);

        return view('artist.edit', [
            'artist' => $artist
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, Artist::$rules);
        
        $artist = Artist::find($id);

        $artist->update($request->all());

        return redirect()->route('artist.index');
    }

    public function delete($id)
    {
        $artist = Artist::where('id', $id);
        $artist->delete();

        return redirect()->back();
    }

    public function show($id)
    {
    	$artist = Artist::find($id);

    	return view('artist.show', ['artist' => $artist]);
    }
}
